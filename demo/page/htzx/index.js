Page({
  data: {
    imgUrls: [
      '../../image/1.jpg',
      '../../image/2.jpg',
      '../../image/8.jpg',
      '../../image/3.jpg',
      '../../image/4.jpg',
      '../../image/5.jpg',
      '../../image/6.jpg',
     
    ],
    indicatorDots: false,
    vertical: false,
    autoplay: true,
    interval: 3000,
    duration: 1200,
    iconArray: [
      {
        "iconUrl": '../../image/virtus.png',
        "iconText": '感冒',
        "id": "ganmao"
      },
      {
        "iconUrl": '../../image/virtus.png',
        "iconText": '不寐', "id": "bumei"
      },
      {
        "iconUrl": '../../image/virtus.png',
        "iconText": '腹痛', "id": "futong"
      },
      {
        "iconUrl": '../../image/virtus.png',
        "iconText": '胃痛', "id": "weitong"
      },
      {
        "iconUrl": '../../image/virtus.png',
        "iconText": '腹泻', "id": "fuxie"
      },
      {
        "iconUrl": '../../image/more.png',
        "iconText": '更多'
      }
    ],
    list: [
      {
        id: 'ganmao',
        name: '感冒',
        open: false,
      }, {
        id: 'weitong',
        name: '胃痛',
        open: false,
        
      }, {
        id: 'bumei',
        name: '不寐',
        open: false,
      }, {
        id: 'futong',
        name: '腹痛',
        open: false,
      }, {
        id: 'fuxie',
        name: '腹泻',
        open: false,
      }
    ]
  },
  kindToggle: function (e) {
    var id = e.currentTarget.id, list = this.data.list;
    for (var i = 0, len = list.length; i < len; ++i) {
      if (list[i].id == id) {
        list[i].open = !list[i].open
      } else {
        list[i].open = false
      }
    }
    this.setData({
      list: list
    });
  },
  cusImageLoad: function (e) {
    var that = this;
    that.setData(WxAutoImage.wxAutoImageCal(e));
  }
})
