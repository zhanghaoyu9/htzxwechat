// page/htzx/result/result.js
var dt = {
  "illname": "牙痛",
  "us": [1613, 1615, 1617, 1618, 1619, 1621]
};
Page({

  /**
   * 页面的初始数据
   */
  data: {
    medicine:[],
    sick:''
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that = this;
    //dt.illname = options.illname;
    //dt.us = options.us
    wx.setNavigationBarTitle({
      title: '诊断结果'

    });
    wx.setNavigationBarColor({
      frontColor: '#ffffff',
      backgroundColor: '#855eef'
    });
    wx.request({
      url: 'http://139.199.159.173:8080/htzx/msmapper',
      method: 'POST',
      headers:{
        "Content-Type": "application/json"  
      },
      data: dt,
      success: function (res) {
        
        for (var i=0; i<res.data.medicines.length; ++i){
          res.data.medicines[i].effect = res.data.medicines[i].effect.slice(0,21);
        }
        that.setData({
          medicine:res.data.medicines,
          sick:res.data.sickname
        });
      },
      fail:function(){
        
      }
    });
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
  
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
  
  }
})