// page/htzx/sickness/sickness.js
var illnessName = {
  '妇产科': {
    'ins': '气之根，肾中真阳也；血之根，肾中真阳也',
    'img': 'majorImg/fuchanke.png', 
    'value': [
      "月经过少",
    "月经后期",
    "月经先期",
    "月经过多",
    "月经先后不定期",
    "阴痒",
    "妊娠肿胀",
    "经期延长",
    "经行头痛",
    "卵巢功能早衰",
    "经行泄泻",
    "妊娠小便淋痛",
    "经行乳房胀痛",
    "经行发热",
    "经间期出血",
    "更年期综合征",
    "黄褐斑",
    "闭经",
    "产后缺乳",
    "产后小便不通",
    "崩漏中医辨证和中成药推荐",
    ]},
  '儿科': { 
  'ins': '', 
  'img': 'majorImg/erke.png', 
  'value': [
  "小儿喘息",
  "小儿发热",
  "小儿便秘",
  "小儿发热",
  "小儿肥胖",
  "小儿腹泻",
  "小儿感冒",
  "小儿咳嗽",
  "小儿湿疹",
  "小儿厌食",
  ] },
'五官科': {
  'ins': '',
  'img':'majorImg/wuguanke.png', 
  'value': [
  "耳鸣耳聋",
  "赤眼",
  "唇风",
  "耳眩晕",
  "流泪症",
  "伤风鼻塞",
  "牙痛",
  "喉痹）",
  "针眼",
  "鼻渊",
  "鼻衄",
  ] },
  '内科': { 
  'ins': '', 
  'img': 'majorImg/neike.png', 
  'value': [
    "痹证",
    "水肿",
    "癃闭",
    "心悸",
    "便秘",
    "喘证",
    "胸痹",
    "泄泻",
    "汗证",
    "腰痛",
    "咳嗽",
    "遗尿",
    "郁证",
    "中风",
    "淋证",
    "感冒",
    "头痛",
    "不寐(失眠)",
  ] },
  '外科': { 
    'ins': '', 
    'img': 'majorImg/waike.png', 
    'value': [
      "粉刺",
      "乳核",
      "乳疬",
      "乳癖",
      "乳岩",
      "乳痈",
      "褥疮",
      "痔疮",
      "斑秃",
      "牛皮癣",
    ] },
}
Page({

  /**
   * 页面的初始数据
   */
  data: {
    illnessName: [],
    ins: '',
    major: '2131231',
    img:''
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    console.log(illnessName)
    console.log(options.major)
    wx.setNavigationBarTitle({
      title:''
      
    })
    wx.setNavigationBarColor({
      frontColor:'#ffffff',
      backgroundColor:'#855eef'
    })
    this.setData({
      illnessName: illnessName[options.major]['value'],
      ins: illnessName[options.major]['ins'],
      major: options.major,
      img: illnessName[options.major]['img']
    })
    console.log(this.data)
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})